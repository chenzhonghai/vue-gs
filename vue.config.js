const path = require("path");

module.exports = {
  // 开发服务器配置
  devServer: {
    port: 9596,
    proxy: {
      "/api": {
        target: "http://121.40.248.159:8292/api",
        changeOrigin: true,
        pathRewrite: {
          // 如果接口本身没有/api需要通过pathRewrite来重写了地址
          "^/api": "/",
        },
      },
    },
  },
  // webpack配置
  configureWebpack: {
    devtool: "source-map", // 生成map文件
    resolve: {
      // 别名
      alias: {
        images: path.resolve(__dirname, "src/assets/images"),
      },
    },
  },
};
