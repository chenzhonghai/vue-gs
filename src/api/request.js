import axios from "axios";
import Store from "@/store/index.js";
// api地址
// const BASE_NODE_BUILD = process.env.VUE_APP_BASE_NODE_BUILD // 构建环境/服务环境
// // 环境地址汇总
// const apiLists = {
//   tour: {
//     kf: 'https://dev-tour-api.apyfc.com',
//     dev: 'https://test-tour-api.apyfc.com',
//     test: 'https://test-tour-api.apyfc.com',
//     prod: 'https://tourapi.apyfc.com',
//     sim: 'https://emulation-tour-api.apyfc.com'
//   },
//   interaction: {
//     kf: 'https://dev-interaction-api.apyfc.com',
//     dev: 'https://test-interaction-api.apyfc.com',
//     test: 'https://test-interaction-api.apyfc.com',
//     prod: 'https://interactionapi.apyfc.com',
//     sim: 'https://emulation-interaction-api.apyfc.com'
//   }
// }

// api请求配置
// const Axios = require('axios')
const service = axios.create({
  timeout: 15000, // 请求超时时间
});

// 请求拦截器
service.interceptors.request.use(
  (config) => {
    // 在请求头插入token
    if (Store.getters.token) {
      config.headers.userToken = Store.getters.token;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

// 数据返回拦截器
service.interceptors.response.use(
  (res) => {
    console.log(res);
    if (res.data.status === -1) {
      this.$message({
        message: "您已经被登出，请重新登录",
        type: "warning",
      }).then(() => {
        // Store.dispatch('FedLogOut')
        this.$router.push({
          name: "/Login",
        });
      });
    }
    return res.data;
  },
  (error) => {
    const status = error.response.status;
    if (status === 500) {
      this.$message({
        type: "error",
        message: `服务器故障 ${error.response.data.Message}`,
      });
    } else if (status === 404) {
      this.$message({
        type: "error",
        message: `${error.response.data.Message}`,
      });
    } else {
      this.$message({ message: error.response.data.msg });
    }
    // 这样返回，可以避免浏览器报 Uncaught (in promise) 错误，不过这样的话在axios单例中就无法单独处理报错
    return new Promise(() => {});
  }
);
