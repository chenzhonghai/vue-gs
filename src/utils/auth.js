// token获取/删除/修改
import Cookies from "js-cookie";

// 截止时间：10天
const ABORT_TIME = new Date(Date.now() + 864000000);

// 根据环境设置cookie的domain
const DOMAIN = process.env.NODE_ENV === "development" ? "" : "apyfc.com";

// 获取token
export function getToken() {
  return Cookies.get("user-Token");
}

// 设置token
export function setToken(token) {
  Cookies.set("user-Token", token, { domain: DOMAIN, expires: ABORT_TIME });
}

// 移除token
export function removeToken() {
  Cookies.remove("user-Token", { domain: DOMAIN, expires: ABORT_TIME });
}
