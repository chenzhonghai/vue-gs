import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {},
  getters,
  strict: process.env.NODE_ENV === "development", // 严格模式
});
