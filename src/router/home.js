// 首页的路由

const _import = require("./_import_" + process.env.NODE_ENV);

export default [
  {
    path: "/",
    name: "mall",
    meta: { requireNav: "home", title: "首页" },
    component: _import("Shop/index"),
  }, //商城首页
  {
    path: "/Community",
    name: "Community",
    meta: { title: "社区", requireAuth: true },
    component: _import("Community/index"),
  }, // 社区首页
  {
    path: "/technical",
    name: "technical",
    meta: { titile: "技术服务" },
    component: _import("Community/Technical/index"),
  }, // 技术服务
];
