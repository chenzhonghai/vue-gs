// 个人中心的路由

const _import = require("./_import_" + process.env.NODE_ENV);

export default [
  {
    path: "/Personal",
    name: "Personal",
    meta: { title: "个人中心", requireAuth: true },
    component: _import("Personal/Index"),
    redirect: "/Personal/Information",
    children: [
      {
        path: "Information",
        name: "Information",
        meta: { title: "个人信息", requireAuth: true },
        component: _import("Personal/Information"),
      },
      {
        path: "Vip",
        name: "Vip",
        meta: { title: "vip会员", requireAuth: true },
        component: _import("Personal/Vip"),
      },
      {
        path: "Message",
        name: "Message",
        meta: { title: "我的消息", requireAuth: true },
        component: _import("Personal/Message"),
      },
      {
        path: "Resource",
        name: "Resource",
        meta: { title: "我的资源", requireAuth: true },
        component: _import("Personal/Resource"),
      },
      {
        path: "Collect",
        name: "Collect",
        meta: { title: "收藏的商品", requireAuth: true },
        component: _import("Personal/Collect"),
      },
      {
        path: "Order",
        name: "Order",
        meta: { title: "我的订单", requireAuth: true },
        component: _import("Personal/Order"),
        children: [
          {
            path: "/Personal/Order",
            name: "OrderList",
            meta: { title: "我的订单", requireAuth: true },
            component: _import("Personal/OrderList"),
          },
          {
            path: "OrderDetails",
            name: "OrderDetails",
            meta: { title: "订单详情", requireAuth: true },
            component: _import("Personal/OrderDetails"),
          },
          {
            path: "Evaluate",
            name: "Evaluate",
            meta: { title: "评价", requireAuth: true },
            component: _import("Personal/Evaluate"),
          },
          {
            path: "ProductReturn",
            name: "ProductReturn",
            meta: { title: "退换货", requireAuth: true },
            component: _import("Personal/ProductReturn"),
            redirect: "ApplyFor",
            children: [
              {
                path: "ApplyFor",
                name: "ApplyFor",
                meta: { title: "退换货申请", requireAuth: true },
                component: _import("Personal/ApplyFor"),
              },
              {
                path: "MerchantConfirmation",
                name: "MerchantConfirmation",
                meta: { title: "商家确认", requireAuth: true },
                component: _import("Personal/MerchantConfirmation"),
              },
              {
                path: "CustomerReturns",
                name: "CustomerReturns",
                meta: { title: "客户退货", requireAuth: true },
                component: _import("Personal/CustomerReturns"),
              },
              {
                path: "ExchangeRefund",
                name: "ExchangeRefund",
                meta: { title: "商家换货/退款", requireAuth: true },
                component: _import("Personal/ExchangeRefund"),
              },
            ],
          },
        ],
      },
      {
        path: "Sample",
        name: "Sample",
        meta: { title: "我的样品", requireAuth: true },
        component: _import("Personal/Sample"),
        children: [
          {
            path: "/Personal/Sample",
            name: "PersonalSample",
            meta: { title: "我的样品", requireAuth: true },
            component: _import("Personal/SampleList"),
          },
          {
            path: "SampleDetails",
            name: "SampleDetails",
            meta: { title: "样品详情", requireAuth: true },
            component: _import("Personal/SampleDetails"),
          },
        ],
      },
      {
        path: "Delivery",
        name: "Delivery",
        meta: { title: "询交期", requireAuth: true },
        component: _import("Personal/Delivery"),
      },
    ],
  },
];
