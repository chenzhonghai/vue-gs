// 社区路由

const _import = require("./_import_" + process.env.NODE_ENV);

export default [
  {
    path: "/searchList",
    name: "searchList",
    meta: { title: "搜索结果" },
    component: _import("Community/searchList"),
  },
  {
    path: "/write",
    name: "write",
    meta: { title: "写文章", requireAuth: true },
    component: _import("Community/WriteArticle"),
  },
  {
    path: "/WriteDraft",
    name: "WriteDraft",
    meta: { title: "文章草稿箱", requireAuth: true },
    component: _import("Community/WriteDraft"),
  },
  {
    path: "/ArticleDetails",
    name: "ArticleDetails",
    meta: { title: "文章详情", requireAuth: true },
    component: _import("Community/ArticleDetails"),
  },
  {
    path: "/QuestionAnswer",
    name: "QuestionAnswer",
    meta: { title: "问答", requireAuth: true },
    component: _import("Community/QuestionAnswer/index"),
  },
  {
    path: "/ask",
    name: "ask",
    meta: { title: "去提问", requireAuth: true },
    component: _import("Community/QuestionAnswer/AskQuestions"),
  },
  {
    path: "/AskDraft",
    name: "AskDraft",
    meta: { title: "问答草稿箱", requireAuth: true },
    component: _import("Community/QuestionAnswer/AskDraft"),
  },
  {
    path: "/QuestionDetails",
    name: "QuestionDetails",
    meta: { title: "问答详情", requireAuth: true },
    component: _import("Community/QuestionAnswer/QuestionDetails"),
  },
  {
    path: "/MessageCenter",
    name: "MessageCenter",
    meta: { title: "消息中心", requireAuth: true },
    component: _import("Community/Message/index"),
  },
  {
    path: "/Comment",
    name: "Comment",
    meta: { title: "评论", requireAuth: true },
    component: _import("Community/Message/Comment"),
  },
  {
    path: "/Focus",
    name: "Focus",
    meta: { title: "关注", requireAuth: true },
    component: _import("Community/Message/Focus"),
  },
  {
    path: "/Like",
    name: "Like",
    meta: { title: "点赞", requireAuth: true },
    component: _import("Community/Message/Like"),
  },
  {
    path: "/PrivateLetters",
    name: "PrivateLetters",
    meta: { title: "私信", requireAuth: true },
    component: _import("Community/Message/PrivateLetters"),
  },
  {
    path: "/PersonalCenter",
    name: "PersonalCenter",
    meta: { title: "个人中心", requireAuth: true },
    component: _import("Community/PersonalCenter/index"),
  },
  {
    path: "/AuthorDetails",
    name: "AuthorDetails",
    meta: { title: "作者详情", requireAuth: true },
    component: _import("Community/PersonalCenter/AuthorDetails"),
  },
  {
    path: "/personalInformation",
    name: "personalInformation",
    meta: { title: "个人资料", requireAuth: true },
    component: _import("Community/PersonalCenter/personalInformation"),
  },
  {
    path: "/Certification",
    name: "Certification",
    meta: { title: "个人认证", requireAuth: true },
    component: _import("Community/PersonalCenter/Certification"),
  },
];
