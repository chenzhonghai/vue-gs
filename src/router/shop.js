// 首页的路由

const _import = require("./_import_" + process.env.NODE_ENV);

export default [
  {
    path: "/Shop/ProductList",
    name: "ProductList",
    meta: { title: "产品列表" },
    component: _import("Shop/ProductList"),
  }, //产品列表
  {
    path: "/Shop/SearchResult",
    name: "SearchResult",
    meta: { title: "搜索结果" },
    component: _import("Shop/SearchResult"),
  }, //搜索结果
  {
    path: "/Shop/Contrast",
    name: "Contrast",
    meta: { title: "商品对比", requireAuth: true },
    component: _import("Shop/Contrast"),
  }, //商品对比
  {
    path: "/Shop/Product",
    name: "Product",
    meta: { title: "商品目录" },
    component: _import("Shop/Product"),
  }, //商品目录
  {
    path: "/Shop/ProductDetail",
    name: "ProductDetail",
    meta: { title: "商品详情" },
    component: _import("Shop/ProductDetail"),
  }, //商品详情
  {
    path: "/Shop/Enquiry",
    name: "Enquiry",
    meta: { title: "批量询价" },
    component: _import("Shop/Enquiry"),
  }, //批量询价
  {
    path: "/Shop/SampleList",
    name: "SampleList",
    meta: { title: "样品清单" },
    component: _import("Shop/Sample/SampleList"),
  }, //样品清单
  {
    path: "/Shop/SampleList/CheckingInformation",
    name: "CheckingInformation",
    meta: { title: "订单填写及核对", requireAuth: true },
    component: _import("Shop/Sample/CheckingInformation"),
  }, //订单填写及核对
  {
    path: "/Shop/SampleList/SampleAddress",
    name: "SampleAddress",
    meta: { title: "收货地址管理", requireAuth: true },
    component: _import("Shop/Sample/SampleAddress"),
  }, //收货地址管理
  {
    path: "/Shop/SampleList/SampleEditAddress",
    name: "SampleEditAddress",
    meta: { title: "编辑收货地址", requireAuth: true },
    component: _import("Shop/Sample/SampleEditAddress"),
  }, //编辑收货地址
  {
    path: "/Shop/SampleList/SampleAddAddress",
    name: "SampleAddAddress",
    meta: { title: "添加收货地址", requireAuth: true },
    component: _import("Shop/Sample/SampleAddAddress"),
  }, //添加收货地址
  {
    path: "/Shop/SampleList/SubmitSuccessfully",
    name: "SampleListSubmit",
    meta: { title: "订单填写及核对", requireAuth: true },
    component: _import("Shop/Sample/SubmitSuccessfully"),
  }, //清单提交成功
  {
    path: "/Shop/ShoppingCart",
    name: "ShoppingCart",
    meta: { title: "我的购物车", requireAuth: true },
    component: _import("Shop/ShoppingCart/ShoppingCart"),
  }, //我的购物车
  {
    path: "/Shop/ShoppingCart/ShoppingOrder",
    name: "ShoppingOrder",
    meta: { title: "订单填写及核对", requireAuth: true },
    component: _import("Shop/ShoppingCart/ShoppingOrder"),
  }, //订单填写及核对
  {
    path: "/Shop/ShoppingCart/ShippingAddress",
    name: "ShippingAddress",
    meta: { title: "收货地址管理", requireAuth: true },
    component: _import("Shop/ShoppingCart/ShippingAddress"),
  }, //收货地址管理
  {
    path: "/Shop/ShoppingCart/ShippingAddress/EditAddress",
    name: "EditAddress",
    meta: { title: "编辑收货地址", requireAuth: true },
    component: _import("Shop/ShoppingCart/EditAddress"),
  }, //编辑收货地址
  {
    path: "/Shop/ShoppingCart/ShippingAddress/AddAddress",
    name: "AddAddress",
    meta: { title: "添加收货地址", requireAuth: true },
    component: _import("Shop/ShoppingCart/AddAddress"),
  }, //添加收货地址
  {
    path: "/Shop/ShoppingCart/SubmitSuccessfully",
    name: "ShoppingCartSubmit",
    meta: { title: "订单提交成功", requireAuth: true },
    component: _import("Shop/ShoppingCart/SubmitSuccessfully"),
  }, //订单提交成功
  {
    path: "/Shop/ShoppingCart/UploadCertificate",
    name: "UploadCertificate",
    meta: { title: "上传支付凭证", requireAuth: true },
    component: _import("Shop/ShoppingCart/UploadCertificate"),
  }, //添加收货地址
  {
    path: "/Shop/BrandSearch",
    name: "BrandSearch",
    meta: { title: "品牌专场" },
    component: _import("Shop/BrandSearch"),
  }, //品牌收缩
];
