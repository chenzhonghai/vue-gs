// 技术服务路由

const _import = require("./_import_" + process.env.NODE_ENV);

export default [
  {
    path: "/Customization",
    name: "Customization",
    meta: { title: "定制化模块" },
    component: _import("Community/Technical/Customization"),
  },
  {
    path: "/AnnouncementList",
    name: "AnnouncementList",
    meta: { title: "最新公告" },
    component: _import("Community/Technical/AnnouncementList"),
  },
  {
    path: "/AnnouncementInfo",
    name: "AnnouncementInfo",
    meta: { title: "最新公告详情" },
    component: _import("Community/Technical/AnnouncementInfo"),
  },
  {
    path: "/QuestionList",
    name: "QuestionList",
    meta: { title: "常见问题" },
    component: _import("Community/Technical/QuestionList"),
  },
  {
    path: "/QuestionInfo",
    name: "QuestionInfo",
    meta: { title: "常见问题详情" },
    component: _import("Community/Technical/QuestionInfo"),
  },
];
