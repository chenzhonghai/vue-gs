import Vue from "vue";
import Router from "vue-router";
import PublicRoute from "./public.js"; // 公共路由
import Home from "./home.js"; // 首页路由
import Technical from "./technical.js"; // 技术服务路由
import Community from "./community.js"; // 论坛路由
import Shop from "./shop.js"; // 商城路由
import Personal from "./personal.js"; // 个人中心路由
// import Product from "./product.js"; // 产品路由
// import Order from "./order.js"; // 订单路由

Vue.use(Router);

const ROUTES = [].concat(
  Home,
  PublicRoute,
  Technical,
  Community,
  Shop,
  Personal
);

// 初始化路由
const ROUTER = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: ROUTES,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});
// 路由前置拦截
ROUTER.beforeEach((to, from, next) => {
  const requireAuth = to.meta.requireAuth;
  //判断页面是否需要登录
  // token直接从localStorage取
  const token = localStorage.getItem("token");
  // 判断该页面是否需要登录
  if (requireAuth) {
    if (token) {
      next();
    } else {
      next({
        path: "/Login",
        query: {
          redirect: to.fullPath,
        },
      });
    }
  }
  next();
});
// 路由后置拦截
ROUTER.afterEach(() => {});

export default ROUTER;

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject)
    return originalPush.call(this, location, onResolve, onReject);
  return originalPush.call(this, location).catch((err) => err);
};
