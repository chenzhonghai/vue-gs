import Vue from "vue";
import App from "./App.vue";
import router from "./router/index.js";
import ElementUi from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import store from "./store/index.js";
import "./assets/css/index.css";
import "./assets/css/element-variables.scss";
import axios from "axios";
import vshare from "vshare";
// import VueAxios from "vue-axios";

Vue.prototype.$axios = axios;

Vue.config.productionTip = false;
Vue.use(vshare);
Vue.use(ElementUi, axios);

// 时间戳格式化：全局过滤器

Vue.filter("dateDiff", function (timestamp) {
  // 补全为13位
  var arrTimestamp = (timestamp + "").split("");
  for (var start = 0; start < 13; start++) {
    if (!arrTimestamp[start]) {
      arrTimestamp[start] = "0";
    }
  }
  timestamp = arrTimestamp.join("") * 1;

  // 数值补0方法
  var zero = function (value) {
    if (value < 10) {
      return "0" + value;
    }
    return value;
  };

  // 直接显示年月日
  return (function () {
    var date = new Date(timestamp);
    return (
      date.getFullYear() +
      "." +
      zero(date.getMonth() + 1) +
      "." +
      zero(date.getDate())
      // + "日"
    );
  })();
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
